package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseMessage;

/**
 * 回复音乐消息
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>12345678</CreateTime>
  <MsgType><![CDATA[music]]></MsgType>
  <Music>
    <Title><![CDATA[TITLE]]></Title>
    <Description><![CDATA[DESCRIPTION]]></Description>
    <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
    <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
    <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId>
  </Music>
</xml>
 * }
 * @author hybo
 *
 */
public class ReplyMusicMessage extends BaseMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8938723975680989822L;

	/**
	 * 回复音乐消息 中的 音乐信息
	 */
	@XStreamAlias("Music")
	private ReplyMusic music;

	public ReplyMusic getMusic() {
		return music;
	}

	public void setMusic(ReplyMusic music) {
		this.music = music;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		
	}

	
}
