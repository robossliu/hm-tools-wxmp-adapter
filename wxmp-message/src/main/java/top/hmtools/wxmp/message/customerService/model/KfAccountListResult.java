package top.hmtools.wxmp.message.customerService.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class KfAccountListResult extends ErrcodeBean {

	List<KfAccountResult> kf_list;

	public List<KfAccountResult> getKf_list() {
		return kf_list;
	}

	public void setKf_list(List<KfAccountResult> kf_list) {
		this.kf_list = kf_list;
	}

	@Override
	public String toString() {
		return "KfAccountListResult [kf_list=" + kf_list + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
