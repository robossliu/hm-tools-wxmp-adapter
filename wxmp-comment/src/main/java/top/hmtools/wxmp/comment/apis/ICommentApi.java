package top.hmtools.wxmp.comment.apis;

import top.hmtools.wxmp.comment.models.ListCommentParam;
import top.hmtools.wxmp.comment.models.ListCommentResult;
import top.hmtools.wxmp.comment.models.OpenCommentParam;
import top.hmtools.wxmp.comment.models.ReplyCommentParam;
import top.hmtools.wxmp.comment.models.UserCommentParam;
import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;

@WxmpMapper
public interface ICommentApi {

	/**
	 * 2.1 打开已群发文章评论（新增接口）
	 * @param openCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/open")
	public ErrcodeBean openComment(OpenCommentParam openCommentParam);

	/**
	 * 2.2 关闭已群发文章评论（新增接口）
	 * @param openCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/close")
	public ErrcodeBean closeComment(OpenCommentParam openCommentParam);
	
	/**
	 * 2.3 查看指定文章的评论数据（新增接口）
	 * @param listCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/list")
	public ListCommentResult listComment(ListCommentParam listCommentParam);
	
	/**
	 * 2.4 将评论标记精选（新增接口）
	 * @param userCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/markelect")
	public ErrcodeBean markelectComment(UserCommentParam userCommentParam);
	
	/**
	 * 2.5 将评论取消精选
	 * @param userCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/unmarkelect")
	public ErrcodeBean unmarkelectComment(UserCommentParam userCommentParam);
	
	/**
	 * 2.6 删除评论（新增接口）
	 * @param userCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/delete")
	public ErrcodeBean deleteComment(UserCommentParam userCommentParam);
	
	/**
	 * 2.7 回复评论（新增接口）
	 * @param replyCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/reply/add")
	public ErrcodeBean replyComment(ReplyCommentParam replyCommentParam);
	
	/**
	 * 2.8 删除回复（新增接口）
	 * @param userCommentParam
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri="/cgi-bin/comment/reply/delete")
	public ErrcodeBean deleteReplyComment(UserCommentParam userCommentParam);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
