package top.hmtools.wxmp.material.model;

import java.util.List;

public class ContentResult {

	private List<Articles> news_item;
	
	/**
	 * 这篇图文消息素材的最后更新时间
	 */
	private long update_time;

	public List<Articles> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<Articles> news_item) {
		this.news_item = news_item;
	}

	public long getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(long update_time) {
		this.update_time = update_time;
	}

	@Override
	public String toString() {
		return "ContentResult [news_item=" + news_item + ", update_time=" + update_time + "]";
	}
	
	
}
