[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-menu.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-menu%22)
#### 前言
本组件对应实现微信公众平台“自定义菜单”章节相关api接口，原接口文档地址：[自定义菜单](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1434698695)

#### 接口说明
- `top.hmtools.wxmp.menu.apis.IMenuApi` 对应实现创建、查询、删除接口
- `top.hmtools.wxmp.menu.apis.IConditionalMenuApi` 对应实现个性化菜单接口相关

#### 事件消息类说明
- `top.hmtools.wxmp.menu.models.eventMessage`包下对应实现“事件推送”相关xml数据对应的Javabean数据结构。可参阅 top.hmtools.wxmp.menu.enums.EMenuEventMessages。

#### 使用示例
0. 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-menu</artifactId>
  <version>1.0.0</version>
</dependency>
```

1. 获取wxmpSession，参照 [wxmp-core/readme.md](../wxmp-core/readme.md)
```
//2 获取动态代理对象实例
IMenuApi menuApi = wxmpSession.getMapper(IMenuApi.class);

//3 获取自定义菜单数据并打印
MenuWapperBean menu = menuApi.getMenu();
System.out.println(JSON.toJSONString(menu));
```

更多示例参见：
- [自定义菜单示例](src/test/java/top/hmtools/wxmp/menu/apis/IMenuApiTest.java)
- [个性化自定义菜单示例](src/test/java/top/hmtools/wxmp/menu/apis/IConditionalMenuApiTest.java)
- [菜单事件推送xml数据解析、处理示例](src/test/java/top/hmtools/wxmp/menu/message/WxmpMessageTest.java)

2020.05.31 自定义菜单组件全部测试OK