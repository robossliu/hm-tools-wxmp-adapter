package top.hmtools.wxmp.menu.models.eventMessage;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 发送的图片信息
 * @author Hybomyth
 *
 */
public class SendPicsInfo {

	/**
	 * 发送的图片数量
	 */
	@XStreamAlias("Count")
	private int count;
	
	/**
	 * 图片列表
	 */
	@XStreamAlias("PicList")
	private List<SendPicItem> picList;

	public int getCount() {
		if(this.picList != null){
			this.count = this.picList.size();
			return count;
		}else{
			return -1;
		}
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<SendPicItem> getPicList() {
		return picList;
	}

	public void setPicList(List<SendPicItem> picList) {
		this.picList = picList;
	}

	@Override
	public String toString() {
		return "SendPicsInfo [Count=" + count + ", PicList=" + picList + "]";
	}
	
	
}
