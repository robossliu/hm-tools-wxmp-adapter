package top.hmtools.wxmp.core.enums;

/**
 * http请求参数数据结果模式类型，即 主要 是 request body 的数据方式
 * @author HyboWork
 *
 */
public enum HttpParamType {

	/**
	 * http request body form
	 * form表单，可以包含文件上传，数据项类型只能为 字符串 或者 文件（输入流）
	 */
	FORM_DATA,

	/**
	 * form表单，不能用于上传文件，因为会转换成
	 * 键值对，比如,name=java&age = 23
	 */
	FORM_DATA_URLENCODED,
	
	/**
	 * http request body json string to stream
	 * 转换成json字符串作为request body 数据流
	 */
	JSON_BODY,
	
	/**
	 * 转换成普通文本字符串作为request body 数据流
	 */
	TEXT_BODY,
	
	/**
	 * 转换成xml字符串作为request body 数据流
	 */
	XML_BODY,
	
	/**
	 * 转换成html字符串作为request body 数据流
	 */
	HTML_BODY,
	
	/**
	 * 转换成原始二进制输入流作为request body 数据流
	 */
	BINARY_BODY
	;
}
