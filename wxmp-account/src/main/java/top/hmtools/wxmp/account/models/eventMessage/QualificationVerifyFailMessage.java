package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 资质认证失败
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442401156</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[qualification_verify_fail]]></Event>  
  <FailTime>1442401122</FailTime>  
  <FailReason><![CDATA[by time]]></FailReason> 
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.qualification_verify_fail)
public class QualificationVerifyFailMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5421540191799104089L;
	
	@XStreamAlias("FailTime")
	private Long failTime;
	
	@XStreamAlias("FailReason")
	private String failReason;
	
	

	public Long getFailTime() {
		return failTime;
	}



	public void setFailTime(Long failTime) {
		this.failTime = failTime;
	}



	public String getFailReason() {
		return failReason;
	}



	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	

	@Override
	public String toString() {
		return "QualificationVerifyFail [failTime=" + failTime + ", failReason=" + failReason + ", event=" + event
				+ ", eventKey=" + eventKey + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName
				+ ", createTime=" + createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
