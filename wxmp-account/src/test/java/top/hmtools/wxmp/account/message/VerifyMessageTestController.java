package top.hmtools.wxmp.account.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.account.models.eventMessage.AnnualRenewMessage;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifyFailMessage;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifySuccessMessage;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifyFailMessage;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifySuccessMessage;
import top.hmtools.wxmp.account.models.eventMessage.VerifyExpiredMessage;
import top.hmtools.wxmp.core.annotation.WxmpController;
import top.hmtools.wxmp.core.annotation.WxmpRequestMapping;

@WxmpController
public class VerifyMessageTestController {

	final Logger logger = LoggerFactory.getLogger(VerifyMessageTestController.class);
	private ObjectMapper objectMapper;
	
	@WxmpRequestMapping
	public void executeMessage(QualificationVerifySuccessMessage msg){
		this.printFormatedJson("1 资质认证成功（此时立即获得接口权限）", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(QualificationVerifyFailMessage msg){
		this.printFormatedJson("2 资质认证失败", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(NamingVerifySuccessMessage msg){
		this.printFormatedJson("3 名称认证成功（即命名成功）", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(NamingVerifyFailMessage msg){
		this.printFormatedJson("4 名称认证失败（这时虽然客户端不打勾，但仍有接口权限", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(AnnualRenewMessage msg){
		this.printFormatedJson("5 年审通知", msg);
	}
	
	@WxmpRequestMapping
	public void executeMessage(VerifyExpiredMessage msg){
		this.printFormatedJson("6 认证过期失效通知", msg);
	}
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
